import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { HeaderMComponent } from './header-m/header-m.component';
import { FooterMComponent } from './footer-m/footer-m.component';
import { ContainComponent } from './contain/contain.component';
import { SidebarMComponent } from './sidebar-m/sidebar-m.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieCardComponent,
    HeaderMComponent,
    FooterMComponent,
    ContainComponent,
    SidebarMComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
